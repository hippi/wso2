#!/bin/bash
GCC_VERSION="4.3"
export R_INCLUDE="/app/vendor/R/lib64/R/include"
export PATH="/app/vendor/R/bin:/app/vendor/.apt/usr/bin:/app/bin:/usr/ruby1.9.2/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:$PATH"
export LD_LIBRARY_PATH="/app/jre/lib/amd64/server:/app/vendor/R/lib/R/library/rJava/jri:/app/vendor/.apt/usr/lib/libblas:/app/vendor/.apt/usr/lib/lapack:/app/vendor/.apt/usr/lib/x86_64-linux-gnu:/app/vendor/.apt/usr/lib/i386-linux-gnu:/app/vendor/.apt/usr/lib:$LD_LIBRARY_PATH"
export LIBRARY_PATH="/app/vendor/.apt/usr/lib/x86_64-linux-gnu:/app/vendor/.apt/usr/lib/i386-linux-gnu:/app/vendor/.apt/usr/lib:$LIBRARY_PATH"
export INCLUDE_PATH="/app/vendor/.apt/usr/include:$INCLUDE_PATH"
export CPATH="$INCLUDE_PATH"
export CPPPATH="$INCLUDE_PATH"
export PKG_CONFIG_PATH="/app/vendor/.apt/usr/lib/x86_64-linux-gnu/pkgconfig:/app/vendor/.apt/usr/lib/i386-linux-gnu/pkgconfig:/app/vendor/.apt/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
export LDFLAGS="-L/app/vendor/.apt/usr/lib/libblas -L/app/vendor/.apt/usr/lib/lapack $LDFLAGS"
export JAVA_HOME="/app"

export R_BASE=/app/vendor/R
export R_HOME=$R_BASE/lib64/R
export JAVA_CPPFLAGS="-I$JAVA_HOME/include -I$JAVA_HOME/include/linux"
export R_INCLUDE=$R_HOME/include
export PATH=$JAVA_HOME/bin:$R_BASE/bin:/app/vendor/gcc-$GCC_VERSION/bin:$PATH

chmod 777 /home/vcap/app/wso2/wso2am-1.9.1/repository/conf/tomcat/catalina-server.xml

chmod 777 /home/vcap/app/wso2/wso2am-1.9.1/repository/deployment/server/jaggeryapps/store/site/conf/site.json

chmod 777 /home/vcap/app/wso2/wso2am-1.9.1/repository/deployment/server/jaggeryapps/publisher/site/conf/site.json
chmod 777 /home/vcap/app/wso2/wso2am-1.9.1/repository/conf/carbon.xml
chmod -R 777 /home/vcap/app/database/
chmod 777 /home/vcap/app/wso2/wso2am-1.9.1/repository/conf/datasources/master-datasources.xml
chmod 777 /home/vcap/app/wso2/wso2am-1.9.1/repository/conf/axis2/axis2.xml
chmod 777 /home/vcap/app/wso2/wso2am-1.9.1/repository/conf/api-manager.xml
chmod 777 /home/vcap/app/wso2/wso2am-1.9.1/repository/conf/identity.xml
chmod 777 /home/vcap/app/wso2/wso2am-1.9.1/repository/deployment/server/jaggeryapps/publisher/site/themes/default/templates/user/login/template.jag
chmod 77 /home/vcap/app/wso2/wso2am-1.9.1/repository/deployment/server/jaggeryapps/store/jagg/jagg.jag
chmod 777 /home/vcap/app/wso2/wso2am-1.9.1/repository/deployment/server/jaggeryapps/store/site/themes/fancy/templates/user/login/js/login.js
chmod 777 /home/vcap/app/wso2/wso2am-1.9.1/repository/deployment/server/jaggeryapps/store/site/themes/fancy/templates/ui/dialogs/template.jag
chmod 777 /home/vcap/app/wso2/wso2am-1.9.1/repository/components/lib/
chmod 777 /home/vcap/app/wso2/wso2am-1.9.1/repository/conf/registry.xml

mv /home/vcap/app/mysql.jar /home/vcap/app/wso2/wso2am-1.9.1/repository/components/lib/
mv /home/vcap/app/master-datasources.xml /home/vcap/app/wso2/wso2am-1.9.1/repository/conf/datasources/
mv /home/vcap/app/carbon/api-manager.xml /home/vcap/app/wso2/wso2am-1.9.1/repository/conf/
mv /home/vcap/app/carbon/carbon.xml /home/vcap/app/wso2/wso2am-1.9.1/repository/conf/
#mv /home/vcap/app/carbon/carbon.xml /home/vcap/app/wso2/wso2am-1.9.1/repository/conf/
mv /home/vcap/app/publisher/template.jag /home/vcap/app/wso2/wso2am-1.9.1/repository/deployment/server/jaggeryapps/publisher/site/themes/default/templates/user/login/
mv /home/vcap/app/identity.xml /home/vcap/app/wso2/wso2am-1.9.1/repository/conf/
mv /home/vcap/app/publisher/jagg.jag  /home/vcap/app/wso2/wso2am-1.9.1/repository/deployment/server/jaggeryapps/store/jagg/
mv /home/vcap/app/publisher/login.js /home/vcap/app/wso2/wso2am-1.9.1/repository/deployment/server/jaggeryapps/store/site/themes/fancy/templates/user/login/js/

mv /home/vcap/app/carbon/template.jag /home/vcap/app/wso2/wso2am-1.9.1/repository/deployment/server/jaggeryapps/store/site/themes/fancy/templates/ui/dialogs/
mv /home/vcap/app/registry.xml  /home/vcap/app/wso2/wso2am-1.9.1/repository/conf/ 
#mv /home/vcap/app/axis2/axis2.xml /home/vcap/app/wso2/wso2am-1.9.1/repository/conf/axis2/
#/home/vcap/app/jre/bin/java -jar /home/vcap/app/start.jar



